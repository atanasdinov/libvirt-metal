FROM python:3-alpine as build

RUN apk add --no-cache build-base linux-headers libvirt-dev openssl apache2-utils

RUN pip wheel sushy_tools libvirt-python

RUN openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -subj "/C=--/L=-/O=-/CN=sushy" -keyout key.pem -out cert.pem

RUN htpasswd -cbB htpasswd.txt admin bmcpass &> /dev/null

FROM python:3-alpine

COPY --from=build *.whl /tmp
RUN pip install /tmp/*.whl

RUN apk add --no-cache libvirt-daemon libvirt libvirt-qemu libvirt-client qemu-system-x86_64
#RUN apk add --no-cache  supervisor qemu-img virt-install

RUN mkdir -p /etc/sushy

COPY --from=build key.pem cert.pem htpasswd.txt /etc/sushy/

WORKDIR /opt

COPY bmc-network.xml storage-pool.xml dom.xml  image.qcow2 entrypoint.sh .

COPY sushy-emulator.conf /etc/sushy/sushy-emulator.conf

CMD ["/opt/entrypoint.sh"]
