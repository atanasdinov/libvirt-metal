#!/bin/sh

/usr/sbin/libvirtd -d
/usr/sbin/virtlogd -d

virsh net-define bmc-network.xml
virsh net-start bmc

# For provisionning and management networks (managed by multus), we want to bridge the VM to multus networks.
# libvirt networks won't work for that purpose as ipa agent is detecting and reporting a callback url to ironic,
# and ironic won't be able to reach the agent neither in NATed network as its address is hidden,
# nor in routed network, as we are not able to inject routes in multus networks.

# So we have to build a bridge by our own and attach it to the pod VM. We also have to manage
# IP allocation: various (multus) CNIs will set pod IPs, but nobody will care about the VMs.

# For that purpose, we use standard linux bridges instead of macvtap, as the latter would require
# to mount the whole devtmpfs in the container, as it creates a /dev/tapXX node that can not be determined in advance.

# host index will be used to determine the allocated IPs (the pod is expected to be run in a statefulset that appends pod index to hostname)
HOST_INDEX=$(hostname | awk -F- '{print $NF}')

enslave_link () {
    # enslave link $1 in bridge $2
    LINK=$1
    BRIDGE=$2
    if ip link show $LINK &>/dev/null; then
        ip link add $BRIDGE type bridge
        ip link set $BRIDGE up
        LINK_ADDR=$(ip -4 -o addr show $LINK | awk '{print $4}')
        ip addr del $LINK_ADDR dev $LINK
        ip link set $LINK master $BRIDGE
        ip addr add $LINK_ADDR dev $BRIDGE
        echo "Link $LINK enslaved in bridge $BRIDGE"
    else
        echo "Link $LINK not found, it won't be enslaved in bridge $BRIDGE"
    fi
}

enslave_link net1 provisioning
enslave_link net2 management

virsh pool-create storage-pool.xml

sed -i "s/NODE_MEM_GB/${NODE_MEM_GB:-4}/" dom.xml

virsh define dom.xml

# Start DHCP to allocate IP on provisioning network
PROVISIONING_MAC=$(virsh domiflist vbmh | awk '$3=="provisioning" {print $5}')
dnsmasq --bind-dynamic \
        --except-interface=lo \
        --interface=provisioning \
        --dhcp-range=192.168.10.10,192.168.10.19,255.255.255.0 \
        --dhcp-option=option:router,192.168.10.1 \
        --dhcp-host=${PROVISIONING_MAC},192.168.10.1${HOST_INDEX},12h \
        --dhcp-no-override \
        --dhcp-authoritative

# Do it also on management network to speed up startup of ipa (even if the address won't be used at this stage)
MANAGEMENT_MAC=$(virsh domiflist vbmh | awk '$3=="management" {print $5}')
dnsmasq --bind-dynamic \
        --except-interface=lo \
        --interface=management \
        --dhcp-range=192.168.100.10,192.168.100.19,255.255.255.0 \
        --dhcp-option=option:router,192.168.100.1 \
        --dhcp-host=${MANAGEMENT_MAC},192.168.100.1${HOST_INDEX},12h \
        --dhcp-no-override \
        --dhcp-authoritative

# Finally start sushy
sushy-emulator --config /etc/sushy/sushy-emulator.conf
