# Libvirt Metal

This project builds an image that emulates baremetal in a container.

It leverages libvirt and sushy-emulator to mimic a baremetal server that can be provisionned with metal3

It is intended to be used in sylva bootstrap cluster, as a sandbox to develop or validate our current stack.

## Getting started

One of the challenges regarding the use of a VM inside a container is network connectivity:

The pod IP can be used by the VM if we use masquerading. However this is not usable for ironic provisionning
because ironic-python-agent is detecting and reporting its provisionning url back to ironic API.

This is why we use two additional networks provisionned by multus that will be used for provisioning and management (kubernetes api) networks.

![libvirt-metal pod](img/libvirt-metal-pod.png)

These networks are managed by multus, and attached to ironic pod, as well as libvirt-metal pods.

In order to have a determinisitc url to reach the sushy virtual bmc url, these pods are deployed using a StatefulSet exposed through HeadLess service, this way each pod will have his own hostname (e.g. `vbmh-0.libvirt-metal.default.svc.cluster.local`), and corresponding bmc url will be:

```
redfish-virtualmedia://vbmh-0.libvirt-metal.default.svc.cluster.local:8000/redfish/v1/Systems/4347b19b-67d0-4f0f-befe-114c0648af0a
```

For now few paramters are fixed as there was no forseenable advantage to make them configurable, but this could be revisisted in the fututre if needed:

- All the VMs share the same fixed UUID in order to have a predictable bmc address (see above)
- BMC password is also fixed
- Bootstrap and management networks have fixed IP ranges (respectively 192.168.10.0/24 and 192.168.100.0/24)

Once provisionned, the management cluster will be accessible from its virtual IP in the management network. Unfortunately, this IP is only accessible in kind container and can't be reached from the host. This is why we have to expose it in kind's bootstrap cluster.

For that purpose we use a service bound to the container IP and without any pod selector, but using a static endpointSlice pointing to management cluster's virtual IP.

The container IP (aka the externaIP of the kubernetes-external service) is used as controlPlaneEndpoint of the cluster, so that kubernetes API will be accessible from the bootstrap and management clusters, and also from the host machine.


![bootstrap networks](img/bootstrap-networks.png)

This image is intended to be deployed and used via k8s manifests defined in [sylva-core libvirt-metal unit](https://gitlab.com/sylva-projects/sylva-core/-/tree/main/kustomize-units/libvirt-metal). It contains the definitions of statefulSet, networks and services that were described above.

## Troubleshooting

You can use the following comand to attach to the virtual machine:

```shell
kubectl exec -it vbmh-0 -- virsh console vbmh
```

And login if you have set passwords for ipa and/or nodes machines.
